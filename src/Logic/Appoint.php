<?php

namespace JyUtils\Logic;
/**
 * 专注于逻辑的 指派算法
 * 用于 根据提供的多个GPS坐标获取相应匹配结果
 * 外卖指派类型
 *  1.规则相关
 *      R4PM (Rider for Promote Member) 把关联用户【下线、朋友........】的订单给骑手
 *      R4AV (Rider for Average ) 平滑所有骑手的订单数量 保持订单都在一个水平线上 不至于有的人多 有的人少
 *  2.位置坐标相关
 *      S2NS (Shop to Near Shop ) 订单的店铺靠近其他正在运行的订单店铺 要求买家相隔距离不到1000米 店铺与店铺的距离不到500米
 *      R2TS (Rider to Touch Shop ) 骑手中途经过其他正在运行订单的店铺距离在200米内
 *      R2TB (Rider to Touch Buy ) 骑手中途经过其他正在运行订单的收货地址 距离在200米内
 *      S2S  (Shop to Shop ) 店到店 骑手订单的店铺里和新订单店铺一样 且 买家收货地址距离不到800米
 *      B2NB  (Buy to Near Buy) 买近买 两个订单的收货地址距离很近 不到400米
 *      B2B  (Buy to  Buy) 买到买 两个订单的收货地址是一样的
 *
 *跑腿指派类型 （A 单 是 新未接订单 B单是已接订单）
 *   位置坐标相关
 *      STS  (Start to Start ) 同起点  终点距离不超过 1200 米
 *      ETE  (End to End )     同终点  起点距离不超过 1200 米
 *      ETS  (End to Start ) B单的终点恰巧是A单的起点 200 米内 且 A单 两点距离小于5000米  且 B单 两点距离小于 3000 米
 *      SNS  (Start Near Start ) B单的路径节点 恰巧经过A单的起点 200米内    且 A单  B单 两终端的距离小于 3000 米
 */

class Appoint
{
    /**
     * R4PM (Rider for Promote Member) 把关联用户【下线、朋友........】的订单给骑手
     * @param int $order_user_id 订单归属用户UID
     * @param array $rider_relateds $rider_relateds 骑手的关联用户 二维数组 key 是 rider_id value 是 关联用户的索引UID数组
     *      ['106'=>['41','56','10068'],'104'=>['58','68']]
     * @return false|int|string  false 表示匹配不到 大于0的整数表示匹配到相关骑手的UID
     */
    public static function R4PM($order_user_id=0,$rider_relateds=[]) {
        foreach ($rider_relateds as $rider_id => $relateds) {
            if (in_array($order_user_id,$relateds)) {
                return $rider_id;
            }
        }
        return false;
    }

    /**
     * R4AV (Rider for Average ) 平滑所有骑手的订单数量 保持订单都在一个水平线上 不至于有的人多 有的人少
     * @param $riders 在线骑手的信息数组
     *      [['rider_id'=>106,'orders'=>18],['rider_id'=>107,'orders'=>5],['rider_id'=>108,'orders'=>3]]
     * @return int 返回匹配订单的骑手UID
     */
    public static function R4AV($riders) {
        sortby($riders, 'orders asc');
        $min_orders = $riders[0]['orders'];
        $riders = array_filter($riders,function ($v) use($min_orders){
            return $v['orders'] == $min_orders;
        });
        // 防止出现多个订单数相同的  取随机
        return $riders[array_rand($riders, 1)]['rider_id'];
    }

    /**
     * S2NS (Shop to Near Shop ) 订单的店铺靠近其他正在运行的订单店铺 要求买家相隔距离不到1000米 店铺与店铺的距离不到500米 手上订单数小于3个
     * @param $order 骑手未接新订单 必须字段 如下
       [
        'id'=>1001,// 订单ID
       'position'=>'116.23793,23.689773',//买家收货地址坐标
       'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
       ]
     * @param $otherOrders  同一区域骑手已接未完成订单列表
        [
            [
            'id'=>1001,// 订单ID
            'rider_id'=>'106',//接单骑手UID
            'position'=>'116.23793,23.689773',//买家收货地址坐标
            'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
            'orders'=>7,//骑手当前手上订单数  可空 空表示不考虑骑手订单数
            ],
             [
            'id'=>1001,// 订单ID
            'rider_id'=>'106',//接单骑手UID
            'position'=>'116.23793,23.689773',//买家收货地址坐标
            'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
            'orders'=>7,//骑手当前手上订单数 可空 空表示不考虑骑手订单数
            ]
      ]
     * @return int|false  大于0 表示匹配到的订单ID  订单ID  订单ID 后续可以根据订单ID 回查 店铺ID 骑手ID 等
     */
    public static function S2NS($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }
            // 订单目的地买家距离
            $buyer_distance = getDistanceGPS($order['position'], $otherOrder['position']);
            // 订单店铺之间的距离
            $shop_distance = getDistanceGPS($order['coordinate'], $otherOrder['coordinate']);

            if ($order_distance < 1000 && $shop_distance < 500) {
                return $otherOrder['id'];
            }
        }
        return false;
    }

    /**
     * R2TS (Rider to Touch Shop ) 骑手中途经过其他正在运行订单的店铺距离在200米内
     * @param $order 骑手未接新订单 必须字段 如下
       [
        'id'=>1001,// 订单ID
       'position'=>'116.23793,23.689773',//买家收货地址坐标
       'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
       ]
     * @param $otherOrders  同一区域骑手已接未完成订单列表
        [
            [
            'id'=>1001,// 订单ID
            'rider_id'=>'106',//接单骑手UID
            'position'=>'116.23793,23.689773',//买家收货地址坐标
            'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
            'orders'=>7,//骑手当前手上订单数  可空 空表示不考虑骑手订单数,
            'roads'=>xxx,//$roads  = $this->_handle_roads($shops); 高德计算返回的重要坐标点集合
            ],
             [
            'id'=>1001,// 订单ID
            'rider_id'=>'106',//接单骑手UID
            'position'=>'116.23793,23.689773',//买家收货地址坐标
            'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
            'orders'=>7,//骑手当前手上订单数 可空 空表示不考虑骑手订单数
            ]
      ]
     * @return int|false  大于0 表示匹配到的订单ID  订单ID  订单ID 后续可以根据订单ID 回查 店铺ID 骑手ID 等
    */
    public static function R2TS($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            if (!is_array($otherOrder['roads'])) {
                $otherOrder['roads'] = json_decode($otherOrder['roads'],true);
            }

            if (!$otherOrder['roads'] || !is_array($roads)) {
                // 没有路径信息 返回
                continue;
            }

            foreach (self::road_lines($otherOrder['roads']) as $line) {
                if (($distance = getDistanceGPS($order['coordinate'], $line)) < 200) {
                   return $otherOrder['id'];
                }
            }
        }
        return false;
    }

    /**
     * R2TB (Rider to Touch Buy ) 骑手中途经过其他正在运行订单的收货地址 距离在200米内
     * @param $order  同 R2TS
     * @param $otherOrders 同 R2TS
     * @return int|false
     */
    public static function R2TB($order,$otherOrders) {
         foreach ($otherOrders as $otherOrder) {
             if (self::passMatch($order,$otherOrder)) {
                 continue;
             }
             if (!is_array($otherOrder['roads'])) {
                 $otherOrder['roads'] = json_decode($otherOrder['roads'],true);
             }

             if (!$otherOrder['roads'] || !is_array($roads)) {
                 // 没有路径信息 返回
                 continue;
             }

             foreach (self::road_lines($otherOrder['roads']) as $line) {
                 if (($distance = getDistanceGPS($order['position'], $line)) < 200) {
                     return $otherOrder['id'];
                 }
             }
         }
         return false;
    }

    /**
     * S2S  (Shop to Shop ) 店到店 骑手订单的店铺里和新订单店铺一样 且 买家收货地址距离不到800米
     * @param $order
     * @param $otherOrders
     * @return false|int
     */
    public static function S2S($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            if (getDistanceGPS($order['coordinate'], $otherOrder['coordinate']) > 20) {
                // 考虑各种系统gps坐标精度不一样 用距离判断 小于20米认为是同一家店铺
                continue;
            }

            if (($distance = getDistanceGPS($order['position'], $otherOrder['position'])) < 800) {
                // 在满足店铺到店铺的条件下  两个订单的收货地址距离要小于800米
                return $otherOrder['id'];
            }
        }
        return false;
    }

    /**
     * B2NB  (Buy to Near Buy) 买近买 两个订单的收货地址距离很近 不到400米
     * @param $order
     * @param $otherOrders
     * @return false|mixed
     */
    public static function B2NB($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            if (($distance = getDistanceGPS($order['position'], $otherOrder['position'])) < 400) {
                return $otherOrder['id'];
            }
        }
        return false;
    }

    /**
     * B2B  (Buy to  Buy) 买到买 两个订单的收货地址是一样的
     * @param $order
     * @param $otherOrders
     * @return false|mixed
     */
    public static function B2B($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            if (($distance = getDistanceGPS($order['position'], $otherOrder['position'])) < 20) {
                // 只要距离很接近就认为是 同一个收货地址
                return $otherOrder['id'];
            }
        }
        return false;
    }

    /*
     * 跑腿模块逻辑
     * */

    /**
     * STS  (Start to Start ) 同起点  终点距离不超过 1200 米
     * @param $order 骑手未接新订单 必须字段 如下
    [
    'id'=>1001,// 订单ID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    ]
     * @param $otherOrders  同一区域骑手已接未完成订单列表
    [
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    'orders'=>7,//骑手当前手上订单数  可空 空表示不考虑骑手订单数
    ],
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    'orders'=>7,//骑手当前手上订单数 可空 空表示不考虑骑手订单数
    ]
    ]
     * @return int|false  大于0 表示匹配到的订单ID
     */
    public static function STS($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }
            
            if (getDistanceGPS($order['start'], $otherOrder['start']) < 200
                && getDistanceGPS($order['end'], $otherOrder['end']) < 1200) {
                //  两个起点距离小于 200米 认为是同起点
                return $otherOrder['id'];
            }
        }
        return false;
    }

    /**
     * ETE  (End to End )     同终点  起点距离不超过 1200 米
     * @param $order 骑手未接新订单 必须字段 如下
    [
    'id'=>1001,// 订单ID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    ]
     * @param $otherOrders  同一区域骑手已接未完成订单列表
    [
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    'orders'=>7,//骑手当前手上订单数  可空 空表示不考虑骑手订单数
    ],
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    'orders'=>7,//骑手当前手上订单数 可空 空表示不考虑骑手订单数
    ]
    ]
     * @return int|false  大于0 表示匹配到的订单ID
     */
    public static function ETE($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            if (getDistanceGPS($order['end'], $otherOrder['end']) < 200
                && getDistanceGPS($order['start'], $otherOrder['start']) < 1200) {
                //  两个终点距离小于 200米 认为是同终点
                return $otherOrder['id'];
            }
        }
        return false;
    }


    /**
     * ETS  (End to Start ) B单的终点恰巧是A单的起点 200 米内 且 A单 两点距离小于5000米  且 B单 两点距离小于 3000 米
     * @param $order 骑手未接新订单 必须字段 如下
    [
    'id'=>1001,// 订单ID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    ]
     * @param $otherOrders  同一区域骑手已接未完成订单列表
    [
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    'orders'=>7,//骑手当前手上订单数  可空 空表示不考虑骑手订单数
    ],
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//结束坐标
    'orders'=>7,//骑手当前手上订单数 可空 空表示不考虑骑手订单数
    ]
    ]
     * @return int|false  大于0 表示匹配到的订单ID
     */
    public static function ETS($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            // B单的终点恰巧是A单的起点 200 米内 且 A单 两点距离小于5000米  且 B单 两点距离小于 3000 米
            if (getDistanceGPS($otherOrder['end'], $order['start']) < 200
                && getDistanceGPS($order['start'], $order['end']) < 5000
                && getDistanceGPS($otherOrder['start'], $otherOrder['end']) < 3000) {
                return $otherOrder['id'];
            }
        }
        return false;
    }

    /**
     *  SNS  (Start Near Start ) B单的路径节点 恰巧经过A单的起点 200米内    且 A单  B单 两终端的距离小于 3000 米
     * @param $order 骑手未接新订单 必须字段 如下
    [
    'id'=>1001,// 订单ID
    'position'=>'116.23793,23.689773',//买家收货地址坐标
    'coordinate'=>'116.23793,23.689773',//订单的店铺坐标  限单店铺
    ]
     * @param $otherOrders  同一区域骑手已接未完成订单列表
    [
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//终点坐标
    'orders'=>7,//骑手当前手上订单数  可空 空表示不考虑骑手订单数,
    'roads'=>xxx,//$roads  = $this->_handle_roads($shops); 高德计算返回的重要坐标点集合
    ],
    [
    'id'=>1001,// 订单ID
    'rider_id'=>'106',//接单骑手UID
    'start'=>'116.23793,23.689773',//起始坐标
    'end'=>'116.23793,23.689773',//终点坐标
    'orders'=>7,//骑手当前手上订单数 可空 空表示不考虑骑手订单数
    'roads'=>xxx,//$roads  = $this->_handle_roads($shops); 高德计算返回的重要坐标点集合
    ]
    ]
     * @return int|false  大于0 表示匹配到的订单ID
     */
    public static function SNS($order,$otherOrders) {
        foreach ($otherOrders as $otherOrder) {
            if (self::passMatch($order,$otherOrder)) {
                continue;
            }

            if (!is_array($otherOrder['roads'])) {
                $otherOrder['roads'] = json_decode($otherOrder['roads'],true);
            }

            if (!$otherOrder['roads'] || !is_array($roads)) {
                // 没有路径信息 返回
                continue;
            }

            // B单的路径节点 恰巧经过A单的起点 200米内    且 A单  B单 两终端的距离小于 3000 米
            foreach (self::road_lines($otherOrder['roads']) as $line) {
                if (getDistanceGPS($order['start'], $line) < 200
                    && getDistanceGPS($order['end'], $otherOrder['end']) < 3000) {
                    return $otherOrder['id'];
                }
            }
        }
        return false;
    }


    /**
     * 判断订单是否不参与匹配
     * @param $newOrder  未接新订单
     * @param $otherOrder 已接订单
     * @return bool  true 放弃匹配 false 参与匹配
     */
    protected static function passMatch($newOrder,$otherOrder) {
        if ($newOrder['id']==$otherOrder['id'] || !$otherOrder['rider_id']) {
            // 相同订单 或者 非配送单 不参与匹配
            return true;
        }

        if (isset($otherOrder['orders']) && $otherOrder['orders']>=3) {
            // 骑手订单数大于等于3
            return true;
        }
        return false;
    }

    protected static function road_lines(array $roads)
    {
        $ret = [];

        $expanded = flat(array_column(flat(array_column($roads['parts'], 'paths'), 1), 'steps'), 1);
        foreach ($expanded as $v) {
            $ret = array_merge($ret, explode(';', $v['polyline']));
        }

        return $ret;
    }
}
