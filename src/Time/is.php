<?php

namespace JyUtils\Time;

/**
 * 判断类
 */
trait is
{
    /**
     * 是否在「时间段内」
     *
     * @param string $tspan     时间段，如："23:00 - 08:00"
     * @param int    $timestamp 要判断的时间戳，留空为当前时间
     * @return bool 在时间段内 true，否则false
     */
    public static function inTimespan($tspan, $timestamp = 0)
    {
        if ($tspan == '') {
            return true;
        }
        $timestamp = $timestamp ?: time();
        $arr       = explode('-', str_replace(' ', '', $tspan));
        if (count($arr) != 2) {
            return true;
        }
        // 12:00 - 18:00
        if ($arr[0] <= $arr[1] && $timestamp >= strtotime($arr[0]) && $timestamp <= strtotime($arr[1])) {
            return true;
        }
        // 22:00 - 02:00
        if ($arr[0] > $arr[1]) {
            if ($timestamp >= strtotime($arr[0]) && $timestamp <= strtotime('23:59:59')) {
                return true;
            } else if ($timestamp >= strtotime('00:00:00') && $timestamp <= strtotime($arr[1])) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 是否在「多个时间段内」
     *
     * @param array $tspans    时间段数组，如：["23:00 - 08:00", "12:00 - 14:00"]
     * @param int   $timestamp 要判断的时间戳，留空为当前时间
     * @return bool 在时间段内 true，否则false
     */
    public static function inTimespans($tspans, $timestamp = 0)
    {
        foreach ($tspans as $tspan) {
            if (self::inTimespan($tspan, $timestamp)) {
                return true;
            }
        }
        return false;
    }
}
