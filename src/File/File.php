<?php

namespace JyUtils\File;

/**
 * 文件操作
 */
class File
{
    /**
     * 读取文件的最后N行
     *
     * @param string $filepath 文件路径
     * @param int    $lines    行数
     * @param int    $skip     跳过行数
     * @param bool   $adaptive 是否自适应
     * @return false|array
     */
    public static function readTailRow($filepath, $lines = 1, $skip = 0, $adaptive = true)
    {
        // Open file
        $f = @fopen($filepath, "rb");
        if (@flock($f, LOCK_SH) === false) return false;
        if ($f === false) return false;
        
        if (!$adaptive) {
            $buffer = 4096;
        } else {
            // Sets buffer size, according to the number of lines to retrieve.
            // This gives a performance boost when reading a few lines from the file.
            $max    = max($lines, $skip);
            $buffer = ($max < 2 ? 64 : ($max < 10 ? 512 : 4096));
        }
        
        // Jump to last character
        fseek($f, -1, SEEK_END);
        
        // Read it and adjust line number if necessary
        // (Otherwise the result would be wrong if file doesn't end with a blank line)
        if (fread($f, 1) == "\n") {
            if ($skip > 0) {
                $skip++;
                $lines--;
            }
        } else {
            $lines--;
        }
        
        // Start reading
        $output = '';
        // While we would like more
        while (ftell($f) > 0 && $lines >= 0) {
            // Figure out how far back we should jump
            $seek = min(ftell($f), $buffer);
            
            // Do the jump (backwards, relative to where we are)
            fseek($f, -$seek, SEEK_CUR);
            
            // Read a chunk
            $chunk = fread($f, $seek);
            
            // Calculate chunk parameters
            $count  = substr_count($chunk, "\n");
            $strlen = mb_strlen($chunk, '8bit');
            
            // Move the file pointer
            fseek($f, -$strlen, SEEK_CUR);
            
            if ($skip > 0) { // There are some lines to skip
                if ($skip > $count) {
                    $skip  -= $count;
                    $chunk = '';
                } // Chunk contains less new line symbols than
                else {
                    $pos = 0;
                    
                    while ($skip > 0) {
                        if ($pos > 0) $offset = $pos - $strlen - 1; // Calculate the offset - NEGATIVE position of last new line symbol
                        else $offset = 0; // First search (without offset)
                        
                        $pos = strrpos($chunk, "\n", $offset); // Search for last (including offset) new line symbol
                        
                        if ($pos !== false) $skip--; // Found new line symbol - skip the line
                        else break; // "else break;" - Protection against infinite loop (just in case)
                    }
                    $chunk = substr($chunk, 0, $pos);    // Truncated chunk
                    $count = substr_count($chunk, "\n"); // Count new line symbols in truncated chunk
                }
            }
            
            if (strlen($chunk) > 0) {
                // Add chunk to the output
                $output = $chunk . $output;
                // Decrease our line counter
                $lines -= $count;
            }
        }
        
        // While we have too many lines
        // (Because of buffer size we might have read too many)
        while ($lines++ < 0) {
            // Find first newline and remove all text before that
            $output = substr($output, strpos($output, "\n") + 1);
        }
        
        // Close file and return
        @flock($f, LOCK_UN);
        fclose($f);
        return explode("\n", trim($output));
    }
}
